import PIL.Image
import PIL.ImageDraw
import math;
import pandas as pd
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import PIL.ImageTk
import traceback
import sys
from PIL import ImageGrab

RESOURCES = "../../resources/"

screen = ImageGrab.grab()

param1 = "main_road"
param2 = "train"
# for run from console
# param1 = sys.argv[1]
# param2 = sys.argv[2]

path = RESOURCES + "gt/" + param1 + "/" + param2 + "_gt.csv"
data = pd.read_csv(path)
oldCondition = 6

if len(data.columns.values) == 6:
    print("overlap column added");
    data['overlap'] = None
    data.to_csv(path, index=False)

firstAbsent = -1;
for i, row in data.iterrows():
    if row['overlap'] == "None" or str(row['overlap']) == 'nan':
        firstAbsent = i
        break;

currentIndex = max(firstAbsent, 0)


class App(Frame):

    def chg_image(self):
        try:
            if self.im.mode == "1":  # bitmap image
                self.img = PIL.ImageTk.BitmapImage(self.im, foreground="white")
            else:  # photo image
                self.img = PIL.ImageTk.PhotoImage(self.im)
            self.la.config(image=self.img, bg="#000000", width=self.img.width(), height=self.img.height())
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def open(self):
        try:
            global currentIndex
            filename = data.iloc[currentIndex]["filename"]

            self.im = PIL.Image.open(RESOURCES + "frames/" + param2 + "/" + filename)

            draw = PIL.ImageDraw.Draw(self.im)
            xFrom = data.iloc[currentIndex]["x_from"]
            yFrom = data.iloc[currentIndex]["y_from"]
            width = data.iloc[currentIndex]["width"]
            height = data.iloc[currentIndex]["height"]
            draw.rectangle(((xFrom - 5, yFrom - 5), (xFrom + width + 5, yFrom + height + 5)), width=3,
                           outline="#ff0000")
            if self.im.width > screen.size[0] or self.im.height > screen.size[1]:
                new_width = min(self.im.width, screen.size[0])
                new_height = min(self.im.height, screen.size[1]) - 155
                self.im = self.im.resize((new_width, new_height), PIL.Image.ANTIALIAS)
            self.chg_image()
            self.num_page = 0
            self.num_page_tv.set("(" + param1 + "/" + param2 + ") " + filename + " (current value: " + str(
                data.iloc[currentIndex]["overlap"]) + ")")
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def seek_prev(self):
        try:
            global currentIndex
            currentIndex = max(currentIndex - 1, 0)
            self.open();

        except:
            messagebox.showerror("Error", traceback.format_exc())

    def seek_next(self):
        try:
            global currentIndex
            currentIndex = min(currentIndex + 1, len(data.index) - 1)
            self.open();
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def save_overlap(self, value):
        try:
            value = int(value);
            if value < -1 or value > 100:
                raise ValueError('Bad value: ' + str(value))

            data.loc[currentIndex, "overlap"] = value;

            data.to_csv(path, index=False)

            # and next
            self.seek_next();
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master.title('Image Viewer')

        self.num_page = 0
        self.num_page_tv = StringVar()

        fram = Frame(self)
        Button(fram, text="GET START", command=self.open).pack(side=LEFT)
        Label(fram, textvariable=self.num_page_tv).pack(side=LEFT)
        entr = Entry(fram, text="0");
        entr.insert(0, "0");
        entr.pack(side=LEFT)
        Button(fram, text="Save overlap",
               command=lambda: [self.save_overlap(entr.get()), entr.delete(0, END), entr.insert(0, "0")]).pack(
            side=LEFT)
        fram.pack(side=TOP, fill=BOTH)
        Button(fram, text="Prev", command=self.seek_prev).pack(side=LEFT)
        Button(fram, text="Next", command=self.seek_next).pack(side=LEFT)

        self.la = Label(self)
        self.la.pack(side="top", fill="both", expand="yes")

        self.pack()


if __name__ == "__main__":
    app = App();
    app.mainloop()
