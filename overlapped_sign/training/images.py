# -*- coding: utf-8 -*-
import os
from datetime import datetime

import cv2
import numpy as np

from stand.ml.emotion_classifier import EmotionClassifier
from stand.utils.logger import Logger


def combine(scene, new_object, mask, top_left):
    """
    Комбинируем новый объект по черной маске
    """
    top, left = top_left
    if mask is None:
        mask = calc_mask(new_object)
    h, w = mask.shape[:2]
    x1 = -left if left < 0 else 0
    y1 = -top if top < 0 else 0
    x2 = scene.shape[1] - left if left + w > scene.shape[1] else w
    y2 = scene.shape[0] - top if top + h > scene.shape[0] else h
    if y1 >= y2 or x1 >= x2:
        # объект за пределами сцены
        return scene

    mask_cut = mask[y1:y2, x1:x2, :]
    new_object_cut = new_object[y1:y2, x1:x2, :]
    sc_y1, sc_x1, sc_y2, sc_x2 = top + y1, left + x1, top + y2, left + x2
    scene_place = scene[sc_y1:sc_y2, sc_x1:sc_x2, :]

    scene[sc_y1:sc_y2, sc_x1:sc_x2, :] = scene_place * (1 - mask_cut) + new_object_cut * mask_cut
    return scene


def calc_mask(new_object):
    """
    Возращает маску по черному цвету
    """
    pre_mask = np.array(new_object < 10, dtype=np.uint8).sum(axis=2)
    mask = np.stack([np.array(pre_mask < 3, dtype=np.uint8)] * 3, axis=2)
    return mask


class FrameSaver:
    def __init__(self, active):
        self._active = active
        self._screen_folder = os.path.join(Logger.logs_folder, 'screenshots')
        self._saving_started = False
        self._folder_name = None
        self._counter = 0

    def save_frame(self, frame, distance, is_best):
        if self._active and self._saving_started:
            self._counter += 1
            curr_time = datetime.now().strftime("%H:%M:%S")
            path = os.path.abspath(os.path.join(self._folder_name,
                                                # f'{curr_time}_{round(distance, 2)}_{"best" if is_best else ""}.jpg'))
                                                f'{self._counter}_{int(distance)}{"_best" if is_best else ""}.jpg'))
            cv2.imwrite(path, frame.get_bgr())

    def reset(self, start=False):
        self._saving_started = start
        if self._active and self._saving_started:
            self._counter = 0
            curr_date = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
            self._folder_name = os.path.abspath(os.path.join(self._screen_folder, curr_date))
            os.makedirs(self._folder_name)


class DetectionsDrawer:
    def __init__(self, config):
        self._alpha = config['bb_transparency']
        self._positive_emo_color = config['positive_emo_color']
        self._bb_color = config['bb_color']
        self._bb_hand = config['bb_hand']

    def run(self, frame):
        scene = frame.get_rgb()
        for face in frame.faces:
            self._draw_bounding_box(face, scene)
        if self._bb_hand and frame.hand['view_position']:
            cv2.rectangle(scene, tuple(frame.hand['view_position'][:2]), tuple(frame.hand['view_position'][2:4]),
                          self._bb_color, 2)
        return scene

    def _draw_bounding_box(self, face, scene):
        emotion, _ = EmotionClassifier.get_dominating(face.emotions)
        left, top, right, bottom = face.rect.tolist()
        bb_color = self._bb_color
        if emotion == 'happy':
            shape = list(face.get_size()) + [3]
            bb_color = self._positive_emo_color
            bb_layer = np.array([bb_color] * face.get_area(), dtype=np.uint8).reshape(shape)
            scene_rect = scene[top:bottom, left:right, :]
            scene[top:bottom, left:right, :] = cv2.addWeighted(scene_rect, 1 - self._alpha, bb_layer, self._alpha, 0)
        cv2.rectangle(scene, (left, top), (right, bottom), bb_color, 2)
