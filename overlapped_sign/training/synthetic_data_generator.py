import os
from random import randint
import cv2
import numpy as np
from random import randint

RESOURCES = "../../resources/"
sample = "train/"
elements = list()
for imgPath in os.listdir(RESOURCES + "elements"):
	print(imgPath)
	elements.append(cv2.imread(RESOURCES + "elements/" + imgPath))

print(len(elements))

count = 0;
for imgPath in os.listdir(RESOURCES + "rtsd-r1/"+sample):
	while True:
	
		back = cv2.imread(RESOURCES + "rtsd-r1/" + sample + imgPath)
		position = randint(0,1)
		overlayCount = randint(1,4)
		for i in range(overlayCount):
			x = np.array([randint(0,48) for i in range(10)])
			y = np.array([randint(0,48) for i in range(10)])

			z = np.polyfit(x, y, 2)
			lspace = np.linspace(0, 50, 5)
			
			draw_x = lspace
			draw_y = np.polyval(z, draw_x)
			
			temp = [draw_x, draw_y]
			
			
			draw_points = (np.asarray([temp[position], temp[1 - position]]).T).astype(np.int32)   # needs to be int32 and transposed

			cv2.polylines(back, [draw_points], False, (randint(0,255),randint(0,255),randint(0,255)), randint(2 if overlayCount == 1 else 1,max(1,4-overlayCount)))
		
		cv2.imwrite(RESOURCES + "rtsd-r1-overlapped/" + sample + imgPath, back) 

		if True:
			break
			
		elemIndex = randint(0, len(elements)-1)

		element = elements[elemIndex]


		(h, w) = element.shape[:2]

		center = (w / 2, h / 2)
		M = cv2.getRotationMatrix2D(center, randint(0, 360), 1)
		rotated = cv2.warpAffine(element, M, (h, w))
		rotated = cv2.resize(rotated, (48, 48))
		
		#cv2.imshow('image', rotated)
		#cv2.waitKey(0)
		

		#img = cv2.add(back, rotated) 
		percent = 0;
		for i in range(48):
			for j in range(48):
				if any(rotated[i][j])>0:
					back[i][j] = rotated[i][j]
					percent = percent + 1
		
		if percent < 48*48/100*10 or percent > 48*48/100*30:
			continue
		
		cv2.imwrite(RESOURCES + "rtsd-r1-overlapped/" + sample + imgPath, back) 
		break
	
	count = count + 1
	print(count)