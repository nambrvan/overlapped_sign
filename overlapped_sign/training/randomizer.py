import pandas as pd
import random

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
    for fileName in fileNames:
        path = "../../resources/gt/" + fileType + "/" + fileName
        full = full.append(pd.read_csv(path))

sample = "train"

path = "../../resources/gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

imgs = set()

for index, row in full.iterrows():
    if int(row['height']) * int(row['width']) > 32 * 32:
        roww = data.loc[data['filename'] == row["filename"]]
        ww = str(roww.get("is_all_set")).split()[1]
        if ww != "NaN":
            if int(float(ww)) == 1:
                imgs.add(row["filename"])

fullSize = len(imgs)
trainSize = int(fullSize / 100 * (100 - 20))
testSize = fullSize - trainSize

print(len(imgs))
print(imgs)
trainImgs = random.sample(imgs, trainSize)
print(len(trainImgs))
print(trainImgs)

uniqueTrain = set()
train = pd.DataFrame()
uniqueTest = set()
test = pd.DataFrame()

for index, row in full.iterrows():
    if int(row['height']) * int(row['width']) > 32 * 32 and row["filename"] in imgs:
        if row["filename"] in trainImgs:
            train = train.append(row)
            uniqueTrain.add(row["filename"])
        else:
            test = test.append(row)
            uniqueTest.add(row["filename"])

print(len(train))
print(len(uniqueTrain))
print(len(test))
print(len(uniqueTest))

train = train.sample(frac=1)
test = test.sample(frac=1)
print(train)
print(test)
train.to_csv("train.csv", index=False)
test.to_csv("test.csv", index=False)
