import PIL.Image
import PIL.ImageDraw
import math;
import pandas as pd 
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import PIL.ImageTk
import traceback
import sys

from PIL import ImageGrab

screen = ImageGrab.grab()

sample = "train"

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = [sample + "_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
	for fileName in fileNames:
		path = "gt/" + fileType + "/" + fileName
		full = full.append(pd.read_csv(path))

full = full.groupby("filename")

path = "gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)


if len(data.columns.values) == 1:
	data['is_all_set'] = None
	data.to_csv(path, index=False)
	print("is_all_set column added");
	
firstAbsent = -1;
for i, row in data.iterrows():
	if row['is_all_set'] == "None" or str(row['is_all_set']) == 'nan':
		firstAbsent = i
		break;

currentIndex = max(firstAbsent, 0)

class App(Frame):
		
	def chg_image(self):
		try:
			if self.im.mode == "1":# bitmap image
				self.img = PIL.ImageTk.BitmapImage(self.im, foreground="white")
			else:# photo image
				self.img = PIL.ImageTk.PhotoImage(self.im)
			self.la.config(image=self.img, bg="#000000", width=self.img.width(), height=self.img.height())
		except:
			messagebox.showerror("Error", traceback.format_exc())

	def open(self):
		try:
			global currentIndex
			filename = data.iloc[currentIndex]["filename"]

			self.im = PIL.Image.open("frames/"+sample+"/" + filename)
			
			draw = PIL.ImageDraw.Draw(self.im)
			rcount = 0;
			if filename in full.groups:
				for index, row in full.get_group(filename).iterrows():
					rcount = rcount + 1;
					xFrom = row["x_from"]
					yFrom = row["y_from"]
					width = row["width"]
					height = row["height"]
					draw.rectangle(((xFrom - 5, yFrom - 5), (xFrom + width + 5, yFrom + height + 5)), width = 3, outline="#ff0000")
			
			if self.im.width>screen.size[0] or self.im.height>screen.size[1]:
				new_width = min(self.im.width, screen.size[0])
				new_height = min(self.im.height, screen.size[1]) - 155
				self.im = self.im.resize((new_width, new_height), PIL.Image.ANTIALIAS)
			self.chg_image()
			self.num_page=0
			self.num_page_tv.set(filename + " (expect: " + str(rcount) + ") " + " (current value: "+str(data.iloc[currentIndex]["is_all_set"])+")")
		except:
			messagebox.showerror("Error", traceback.format_exc())
			
	def seek_prev(self):
		try:
			global currentIndex
			currentIndex = max(currentIndex - 1, 0)
			self.open();
		except:
			messagebox.showerror("Error", traceback.format_exc())

	def seek_next(self):
		try:
			global currentIndex
			currentIndex = min(currentIndex + 1, len(data.index) - 1)
			self.open();
		except:
			messagebox.showerror("Error", traceback.format_exc())
		
	def all_setted(self):
		try:
			data.loc[currentIndex, "is_all_set"] = 1;
			data.to_csv(path, index=False)
			#and next
			self.seek_next();
		except:
			messagebox.showerror("Error", traceback.format_exc())

	def partially_setted(self):
		try:
			data.loc[currentIndex, "is_all_set"] = 0;
			data.to_csv(path, index=False)
			#and next
			self.seek_next();
		except:
			messagebox.showerror("Error", traceback.format_exc())


	def __init__(self, master=None):
		Frame.__init__(self, master)
		self.master.title('Image Viewer')

		self.num_page=0
		self.num_page_tv = StringVar()

		fram = Frame(self)
		Button(fram, text="GET START", command=self.open).pack(side=LEFT)
		Label(fram, textvariable=self.num_page_tv).pack(side=LEFT)


		Button(fram, text="All", command=self.all_setted).pack(side=LEFT)
		Button(fram, text="Partially", command=self.partially_setted).pack(side=LEFT)

		fram.pack(side=TOP, fill=BOTH)
		Button(fram, text="Prev", command=self.seek_prev).pack(side=LEFT)
		Button(fram, text="Next", command=self.seek_next).pack(side=LEFT)
		
		
		self.la = Label(self)
		self.la.pack(side="top", fill="both", expand="yes")

		self.pack()

if __name__ == "__main__":
	app = App(); app.mainloop()