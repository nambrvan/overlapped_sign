from recognize.utils import label_map_util
from recognize.frame import Frame
import tensorflow.compat.v1 as tf
# import tensorflow as tf
import cv2

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
tf.Session(config=config)
import numpy as np


class FasterRCNN:

    def __init__(self, model_path, labels_path, num_classes, handlers=[]):
        self.model_path = model_path
        label_map = label_map_util.load_labelmap(labels_path)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=num_classes,
                                                                    use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)

        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(model_path, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

            self.sess = tf.Session(graph=detection_graph)

        self.out = (
            detection_graph.get_tensor_by_name('image_tensor:0'),
            detection_graph.get_tensor_by_name('detection_boxes:0'),
            detection_graph.get_tensor_by_name('detection_scores:0'),
            detection_graph.get_tensor_by_name('detection_classes:0'),
            detection_graph.get_tensor_by_name('num_detections:0'))

        self.handlers = handlers

    def detect_image(self, path_to_image):
        image_tensor, detection_boxes, detection_scores, detection_classes, num_detections = self.out

        image = cv2.imread(path_to_image)
        image_expanded = np.expand_dims(image, axis=0)
        (boxes, scores, classes, num) = self.sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: frame_expanded})
        for handler in self.handlers:
            handler.handle(Frame(frame, video.get(cv2.CAP_PROP_POS_MSEC)), boxes, scores, classes, num, )
        pass

    def detect_video(self, path_to_video):
        image_tensor, detection_boxes, detection_scores, detection_classes, num_detections = self.out

        video = cv2.VideoCapture(path_to_video)
        skip_frames_counter = 0
        frame_flag = 10
        while video.isOpened():
            ret, frame = video.read()
            if not ret:
                break

            skip_frames_counter = skip_frames_counter + 1
            if not (skip_frames_counter % frame_flag == 0):
                continue

            frame_expanded = np.expand_dims(frame, axis=0)
            (boxes, scores, classes, num) = self.sess.run(
                [detection_boxes, detection_scores, detection_classes, num_detections],
                feed_dict={image_tensor: frame_expanded})

            for handler in self.handlers:
                handler.handle(Frame(frame, video.get(cv2.CAP_PROP_POS_MSEC)), np.squeeze(boxes), np.squeeze(scores),
                               np.squeeze(classes), num, skip_frames_counter)

            if cv2.waitKey(1) == ord('q'):
                break

        # Clean up
        video.release()
        cv2.destroyAllWindows()
        for handler in self.handlers:
            handler.close();
        pass

    def add_handler(self, handler):
        self.handlers.append(handler)
