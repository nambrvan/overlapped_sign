from handler.diplay_handler import DisplayHandler
from handler.log_handler import LogHandler
from handler.video_handler import VideoRecordHandler
from faster_rcnn import FasterRCNN
import logging
from pathlib import Path
import tensorflow as tf
import time

timeIn = time.time()

MODELS_DIR = Path('../../model_exported_graphs/')
model_name =  'inference_graph_20200405'
model_dir = MODELS_DIR / model_name

graph = model_dir / 'frozen_inference_graph.pb'
labelmap = '../../data/object-detection.pbtxt'
classes_num = 1

classification_model = tf.keras.models.load_model('../../models/v2/sign_classification_model.h5')
classification_model = tf.keras.models.load_model('../../models/v2/sign_classification_model.92%.h5')

detector = FasterRCNN(str(graph), str(labelmap), classes_num)

display_handler = DisplayHandler(detector.category_index, classification_model)
detector.add_handler(display_handler)

#log_handler = LogHandler("detection.log")
#detector.add_handler(log_handler)

fileName = "../../resources/video/20160101000418027.MP4"
fileName = "../../resources/video/OPQQ1226.MP4"
fileName = "../../resources/video/AAIR4846.MP4"
outputFileName = fileName+'_output.MP4'


#video_record_handler = VideoRecordHandler(outputFileName, detector.category_index, fileName, classification_model)
#detector.add_handler(video_record_handler)

for handler in detector.handlers:
	logging.info(handler)


detector.detect_video(fileName)

print("time", time.time() - timeIn)