import matplotlib.pyplot as plt
import numpy as np



def overlapPercent():
	return 20


def plot_image(i, predictions_array, true_label, img, class_names, image_name):
  predictions_array, true_label, img, image_name = predictions_array[i], true_label[i], img[i], image_name[i]
  print(i, predictions_array, true_label, class_names, image_name)
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])

  plt.imshow(img, cmap=plt.cm.binary)

  predicted_label = np.argmax(predictions_array)
  if predicted_label == true_label:
    color = 'blue'
  else:
    color = 'red'

  plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                100*np.max(predictions_array),
                                class_names[true_label]),
                                color=color)

def plot_value_array(i, predictions_array, true_label):
  predictions_array, true_label = predictions_array[i], true_label[i]
  plt.grid(False)
  plt.xticks([])
  plt.yticks([])
  thisplot = plt.bar(range(2), predictions_array, color="#777777")
  plt.ylim([0, 1])
  predicted_label = np.argmax(predictions_array)

  thisplot[predicted_label].set_color('red')
  thisplot[true_label].set_color('blue')


#валидационные данные непосредственно в срезе классов
def prepareEvalData(train_images,train_labels,test_images,test_labels,clazz):
	eval_images = [];
	eval_labels = [];
	for i in range(len(train_labels)):
		if train_labels[i] == clazz:
			eval_images.append(train_images[i])
			eval_labels.append(train_labels[i])
	for i in range(len(test_labels)):
		if test_labels[i] == clazz:
			eval_images.append(test_images[i])
			eval_labels.append(test_labels[i])
	eval_images = np.asarray(eval_images)
	eval_labels = np.asarray(eval_labels)
	return np.asarray(eval_images), np.asarray(eval_labels)


def calcTrueResultByClass(predictions, eval_labels, clazz):
	total_eval_count = 0;
	tp_eval_count = 0;
	for i in range(len(eval_labels)):
		total_eval_count = total_eval_count + 1
		if np.argmax(predictions[i]) == eval_labels[i]:
			tp_eval_count = tp_eval_count + 1

	print("TRUE RESULT FOR",clazz, tp_eval_count/total_eval_count, "(",tp_eval_count,"/",total_eval_count,")")