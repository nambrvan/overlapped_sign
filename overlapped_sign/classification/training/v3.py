import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_datasets as tfds
import time
from tensorflow.keras.models import save_model
import random

tfds.disable_progress_bar()

RESOURCES = "../../../" + "resources/"
SAMPLE = "train/"
MODEL_FOLDER = '../../../models/v3/temp/'
CLASS_NAMES = ['Normal', 'Overlapped']

all_images = []
all_labels = []
# закидываем закрытые
# cur_folder = RESOURCES + "rtsd-r1-overlapped/" + SAMPLE
cur_folder = RESOURCES + "v3/overlapped/"
cur_class = 1
images = [os.path.abspath(os.path.join(cur_folder, p)) for p in os.listdir(cur_folder)]
all_images.extend(images)
all_labels.extend([cur_class for _ in range(len(images))])
# закидываем открытые
# cur_folder = RESOURCES + "rtsd-r1/" + SAMPLE
cur_folder = RESOURCES + "v3/normal/"
cur_class = 0
images = [os.path.abspath(os.path.join(cur_folder, p)) for p in os.listdir(cur_folder)]
images = random.sample(images, len(all_images)) #если количество открытых больше, чем закрытых, то рандомно выбираем по количеству закрытых
all_images.extend(images)
all_labels.extend([cur_class for _ in range(len(images))])

dataset = tf.data.Dataset.from_tensor_slices((tf.constant(all_images), tf.constant(all_labels))).shuffle(
    buffer_size=len(all_images))

train_size = int(0.75 * len(all_images))
val_size = int(0.25 * len(all_images))
test_size = int(0.0 * len(all_images))

raw_train = dataset.take(train_size)
remaining = dataset.skip(train_size)
raw_validation = remaining.take(val_size)
raw_test = remaining.skip(test_size)

print(raw_train)
print(raw_validation)
print(raw_test)

for image, label in raw_train.take(2):
    image_string = tf.io.read_file(image)
    image_decoded = tf.image.decode_jpeg(image_string, channels=3)
    plt.figure()
    plt.imshow(image_decoded)
    plt.title(CLASS_NAMES[label])

IMG_SIZE = 48  # All images will be resized to


def format_example(filename, label):
    image_string = tf.io.read_file(filename)
    image_decoded = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.cast(image_decoded, tf.float32)
    image = (image / 127.5) - 1
    image = tf.image.resize(image, (IMG_SIZE, IMG_SIZE))
    return image, label


train = raw_train.map(format_example)
validation = raw_validation.map(format_example)
test = raw_test.map(format_example)

print(train)

BATCH_SIZE = 32
SHUFFLE_BUFFER_SIZE = 1000

train_batches = train.shuffle(SHUFFLE_BUFFER_SIZE).batch(BATCH_SIZE)
validation_batches = validation.batch(BATCH_SIZE)
test_batches = test.batch(BATCH_SIZE)

for image_batch, label_batch in train_batches.take(1):
    pass
image_batch.shape

IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 3)

# Create the base model from the pre-trained model MobileNet V2
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                               include_top=False,
                                               weights='imagenet')
feature_batch = base_model(image_batch)
print(feature_batch.shape)

base_model.trainable = False
base_model.summary()

global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
feature_batch_average = global_average_layer(feature_batch)
print(feature_batch_average.shape)

prediction_layer = tf.keras.layers.Dense(len(CLASS_NAMES))
prediction_batch = prediction_layer(feature_batch_average)
print(prediction_batch.shape)

model = tf.keras.Sequential([
    base_model,
    global_average_layer,
    prediction_layer
])

base_learning_rate = 0.0001
model.compile(optimizer=tf.keras.optimizers.Adam(lr=base_learning_rate),
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.summary()

len(model.trainable_variables)

initial_epochs = 500
validation_steps = 20

loss0, accuracy0 = model.evaluate(validation_batches, steps=validation_steps)

print("initial loss: {:.2f}".format(loss0))
print("initial accuracy: {:.2f}".format(accuracy0))

history = model.fit(train_batches,
                    epochs=initial_epochs,
                    validation_data=validation_batches)

modelName = MODEL_FOLDER + str(int(round(time.time() * 1000))) + '_' + str(
    round(history.history['val_accuracy'][-1], 5)) + '_' + str(
    round(history.history['accuracy'][-1], 5)) + '.h5'
save_model(model, modelName)

predictions = model.predict(test_batches)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()), 1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0, 1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.savefig(modelName + '_loss.png')
plt.show()
