from __future__ import absolute_import, division, print_function, unicode_literals
# TensorFlow и tf.keras
import tensorflow as tf
from tensorflow import keras
# Вспомогательные библиотеки
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image
import util
from tensorflow.keras.models import save_model
import random, os
import time
from tensorflow.keras.callbacks import Callback

#IMAGES_PATH = "../../../" + "resources/"
RESOURCES = "../../../" + "resources/"
MODEL_FOLDER = '../../../models/v2/temp/'

class WeightsSaver(Callback):
	def __init__(self, N, prefix):
		self.N = N
		self.batch = 0
		self.prefix = prefix

	def on_epoch_end(self, batch, logs={}):
		if self.batch % self.N == 0 and self.batch>0:
			name = (MODEL_FOLDER + self.prefix + '_%08d.h5') % self.batch
			save_model(model, name)
		self.batch += 1
		
def splitToImagesAndLabels(data):
	imagesArray = []
	labelsArray = np.array([], dtype=np.uint8)
	for index, row in data.iterrows():
		if len(row) == 0:
			continue
		labelsArray = np.append(labelsArray, row["clazz"]) #0 - normal, 1 - overlapped
		im = Image.open(RESOURCES + row["filename"])
		im2arr = np.array(im)
		im2arr = im2arr[:, :, 0]
		imagesArray.append(im2arr)
	return np.asarray(imagesArray), np.asarray(labelsArray)

def doZip():
	allData = pd.DataFrame(columns=["filename", "clazz"])
	sample = "train/"
	curFolder = "rtsd-r1/" + sample
	for imgPath in os.listdir(RESOURCES + curFolder):
		allData = allData.append({'filename': (curFolder + imgPath), 'clazz': "0"}, ignore_index=True)
	curFolder = "rtsd-r1-overlapped/" + sample
	for imgPath in os.listdir(RESOURCES + curFolder):
		allData = allData.append({'filename': (curFolder + imgPath), 'clazz': "1"}, ignore_index=True)
	allData = allData.sample(frac = 1).sample(frac = 1)
	allData.to_csv("all_rtsd_data.csv", index=False)

count = 0

while True:
	count = count+1
	print(count)
	doZip()

	train_data = pd.DataFrame()
	test_data = pd.DataFrame()
	curAmount = 0
	allData = pd.read_csv("all_rtsd_data.csv")
	for index, row in allData.iterrows():
		if curAmount < int(len(allData)*75/100):
			train_data = train_data.append(row)
		else:
			test_data = test_data.append(row)
		curAmount = curAmount + 1
			
	print(len(train_data))
	print(len(test_data))

	#train_data = pd.read_csv("train.csv")
	#test_data = pd.read_csv("test.csv")

	train_images, train_labels = splitToImagesAndLabels(train_data)
	test_images, test_labels = splitToImagesAndLabels(test_data)

	#fashion_mnist = keras.datasets.fashion_mnist

	#(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()


	class_names = ['Normal', 'Overlapped']

	print(train_images.shape)
	print(test_images.shape)

	print(len(train_labels))
	print(len(test_labels))

	print(class_names)
	print(len(train_labels[train_labels==0]))
	print(len(train_labels[train_labels==1]))
	print(len(test_labels[test_labels==0]))
	print(len(test_labels[test_labels==1]))
	print(len(train_labels[train_labels==0])+len(test_labels[test_labels==0]))
	print(len(train_labels[train_labels==1])+len(test_labels[test_labels==1]))


	train_images = (train_images / 255.0)

	test_images = (test_images / 255.0)


	model = keras.Sequential([
		keras.layers.Flatten(input_shape=(len(train_images[0]), len(train_images[0]))),
		keras.layers.Dense(128, activation='relu'),
		keras.layers.Dense(len(class_names), activation='softmax')
	])

	model.compile(optimizer='adam',
				  loss='sparse_categorical_crossentropy',
				  metrics=['accuracy'])
				  
	history = model.fit(train_images, train_labels, epochs=30000, validation_data=(test_images,  test_labels), callbacks=[WeightsSaver(50, str(count))])
	modelName = MODEL_FOLDER+str(count)+'_'+str(int(round(time.time() * 1000)))+'_'+str(round(history.history['val_accuracy'][-1], 5))+'_'+str(round(history.history['accuracy'][-1], 5))+'_sign_classification_model.h5'
	save_model(model, modelName)
	#plotting graphs for accuracy 
	plt.clf()
	plt.figure(count*2)
	plt.plot(history.history['accuracy'], label='training accuracy')
	plt.plot(history.history['val_accuracy'], label='val accuracy')
	plt.title('Accuracy')
	plt.xlabel('epochs')
	plt.ylabel('accuracy')
	plt.legend()
	#plt.show()
	plt.savefig(modelName + '_accuracy.png')

	plt.figure(count*2+1)
	plt.plot(history.history['loss'], label='training loss')
	plt.plot(history.history['val_loss'], label='val loss')
	plt.title('Loss')
	plt.xlabel('epochs')
	plt.ylabel('loss')
	plt.legend()
	#plt.show()
	plt.savefig(modelName + '_loss.png')

#----

test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print(test_loss, test_acc)
predictions = model.predict(test_images)


def plot_image(i, predictions_array, true_label, img):
	predictions_array, true_label, img = predictions_array[i], int(true_label[i]), img[i]
	plt.grid(False)
	plt.xticks([])
	plt.yticks([])

	plt.imshow(img, cmap=plt.cm.binary)

	predicted_label = np.argmax(predictions_array)
	if predicted_label == true_label:
		color = 'blue'
	else:
		color = 'red'

	plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
								100*np.max(predictions_array),
								class_names[true_label]),
								color=color)

def plot_value_array(i, predictions_array, true_label):
	predictions_array, true_label = predictions_array[i], int(true_label[i])
	plt.grid(False)
	plt.xticks([])
	plt.yticks([])
	thisplot = plt.bar(range(2), predictions_array, color="#777777")
	plt.ylim([0, 1])
	predicted_label = np.argmax(predictions_array)

	thisplot[predicted_label].set_color('red')
	thisplot[true_label].set_color('blue')
  
num_rows = 5
num_cols = 3
num_images = num_rows*num_cols
plt.figure(figsize=(2*2*num_cols, 2*num_rows))
for i in range(num_images):
	plt.subplot(num_rows, 2*num_cols, 2*i+1)
	plot_image(i, predictions, test_labels, test_images)
	plt.subplot(num_rows, 2*num_cols, 2*i+2)
	plot_value_array(i, predictions, test_labels)
plt.show()