import pandas as pd
import cv2
import time
import util
import os

timeIn = time.time()

toRootPath = "../../../"

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
	for fileName in fileNames:
		path = toRootPath + "resources/gt/" + fileType + "/" + fileName
		full = full.append(pd.read_csv(path))




sample = "train"

path = toRootPath + "resources/gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

def getAvailibleImgs():
	imgs = set()

	for index, row in full.iterrows():
		if row["overlap"]>=util.overlapPercent():
			roww = data.loc[data['filename'] == row["filename"]]
			ww = str(roww.get("is_all_set")).split()[1]
			if ww != "NaN":
				if int(float(ww)) == 1:
					imgs.add(row["filename"])
	return imgs

#img = getAvailibleImgs();

temp = pd.DataFrame()

for index, row in full.iterrows():
	#if row["filename"] in imgs:
	#	temp = temp.append(row)
	temp = temp.append(row) #для проверки "просмотрено" раскомментировать выше две строчки!
	
full = temp.sort_values(by=["filename"])

temp = pd.DataFrame()
for index, row in full.iterrows():
	if row["overlap"]<util.overlapPercent() and row["overlap"] > 0:
		continue;
	img = cv2.imread(toRootPath + "resources/frames/" + sample +"/"+ row["filename"])
	y = int(row["y_from"])
	h = int(row["height"])
	x = int(row["x_from"])
	w = int(row["width"])
	if w<30 or h<30:
		continue
	cropedName = row["filename"].split(".")[0]+"_"+str(x)+"_"+str(y)+".jpg";
	savePath = toRootPath + "resources/frames_classification/" + cropedName;
	if os.path.exists(savePath):
		continue;
	crop_img = img[y:y+h, x:x+w]
	crop_img = cv2.resize(crop_img, (48, 48)) # TODO попробовать поиграться с размерами
	cv2.imwrite(savePath, crop_img)
	row["croped"] = cropedName
	temp = temp.append(row)

full = temp.sample(frac=1)



fullSize = len(full);
trainSize = int(fullSize / 100 * (100-20))
testSize = fullSize - trainSize;

print(fullSize);
print(trainSize);
print(testSize);


train = pd.DataFrame();
test = pd.DataFrame();

trainItrsCount = 0;
for index, row in full.iterrows():
	trainItrsCount = trainItrsCount + 1
	train = train.append(row);
	if len(train)>=trainSize:
		break;

testItrsCount = 0;

for index, row in full.iterrows():
	testItrsCount = testItrsCount + 1
	if testItrsCount<=trainItrsCount:
		continue;
	test = test.append(row);


print(len(train))
print(len(test))

train.to_csv("train.csv", index=False)
test.to_csv("test.csv", index=False)

print("total time:", time.time() - timeIn)