import cv2
import pandas as pd

RESOURCES = "../../resources/"
IMG_SIZE = 48
sample = "train"

path = RESOURCES + "gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
    for fileName in fileNames:
        path = RESOURCES + "gt/" + fileType + "/" + fileName
        full = full.append(pd.read_csv(path))

print(len(full))
print(len(full.loc[full['overlap'] > 0]))

countOverlapped = 0
countNormal = 0
badImgs = set()
imgs = set()

forPredict = {}
forPredictNormal = []
inPacket = 20
for index, row in full.iterrows():
    if row["overlap"] <= 10:
        continue
    if row["width"] < 30 and row["height"] < 30:
        continue
    roww = data.loc[data['filename'] == row["filename"]]
    ww = str(roww.get("is_all_set")).split()[1]
    if ww != "NaN":
        if int(float(str(roww.get("is_all_set")).split()[1])) == 1:
            imgs.add(row["filename"])
            if row["overlap"] > 0 and row["overlap"] < 101:
                key = row["overlap"] // inPacket
                if key not in forPredict:
                    forPredict.update({key: pd.DataFrame()})
                forPredict[key] = forPredict[key].append(row)
                countOverlapped = countOverlapped + 1
            elif row["overlap"] == 0:
                forPredictNormal.append(row)
                countNormal = countNormal + 1
        else:
            badImgs.add(row["filename"])

print("bad imgs", len(badImgs))
print("good imgs", len(imgs))

print(countNormal)
print(countOverlapped)

for key in sorted(forPredict.keys()):
    for index, row in forPredict[key].iterrows():
        img = cv2.imread(RESOURCES + "frames/" + sample + "/" + row["filename"])
        y, h, x, w = int(row["y_from"]), int(row["height"]), int(row["x_from"]), int(row["width"])

        crop_img = img[y:y + h, x:x + w]
        crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))
        cv2.imwrite(
            RESOURCES + "v3/overlapped/" + str(x) + "_" + str(y) + "_" + row["filename"],
            crop_img)
# ============================================

# print("открытые:")
# for row in forPredictNormal:
#     img = cv2.imread(RESOURCES + "frames/" + sample + "/" + row["filename"])
#     y, h, x, w = int(row["y_from"]), int(row["height"]), int(row["x_from"]), int(row["width"])
#
#     crop_img = img[y:y + h, x:x + w]
#     crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))
#     cv2.imwrite(
#         RESOURCES + "frames_4test/normal/" + str(x) + "_" + str(y) + "_" + row["filename"], crop_img)
