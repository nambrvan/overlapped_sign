import pandas as pd
import tensorflow as tf 
from tensorflow import keras
import PIL.Image as Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import time

LOWEST_BORDER = 0.8;

RESOURCES = "../../resources/"

sample = "train"

path = RESOURCES + "gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
	for fileName in fileNames:
		path = RESOURCES + "gt/" + fileType + "/" + fileName
		full = full.append(pd.read_csv(path))

print(len(full))
print(len(full.loc[full['overlap'] > 0]))

#full = full.groupby("filename")

countOverlapped = 0;
countNormal = 0;
badImgs = set()
imgs = set()

forPredict = {}
forPredictNormal = []
inPacket = 10
for index, row in full.iterrows():
	#if row["width"] < 35 or row["height"] < 35:
	#	continue
	roww = data.loc[data['filename'] == row["filename"]]
	ww = str(roww.get("is_all_set")).split()[1]
	if ww != "NaN":
		if int(float(str(roww.get("is_all_set")).split()[1])) == 1:
			imgs.add(row["filename"])
			if row["overlap"] > 0 and row["overlap"] < 101:
				key = row["overlap"]//inPacket
				if key not in forPredict:
					forPredict.update({key: pd.DataFrame()})
				forPredict[key] = forPredict[key].append(row);
				countOverlapped = countOverlapped + 1
			elif row["overlap"] == 0:
				forPredictNormal.append(row);
				countNormal = countNormal + 1
		else:
			badImgs.add(row["filename"])

print("bad imgs", len(badImgs))
print("good imgs", len(imgs))

print(countNormal)
print(countOverlapped)

classification_model = tf.keras.models.load_model('../../models/v2/4_1588853110873_0.61523_0.69619_sign_classification_model.h5')
classification_model = tf.keras.models.load_model('../../models/v2/1_1589762605123_0.69084_0.93623_sign_classification_model.h5')

timeInOverlappedPredict = time.time()

plt.figure(0)
NEED_CLASS = 1 #overlapped
total = 0
totalPosCount = 0
totalRealPosCount = 0
totalRealNegCount = 0
for key in sorted(forPredict.keys()):
	total = total + len(forPredict[key])
	
	imagesArray = []
	for index, row in forPredict[key].iterrows():
		#im = Image.open(RESOURCES + 'frames/train/' + row["filename"])
		img = cv2.imread(RESOURCES + "frames/" + sample +"/"+ row["filename"])
		y,h,x,w = int(row["y_from"]),int(row["height"]),int(row["x_from"]),int(row["width"])
		
		crop_img = img[y:y+h, x:x+w]
		crop_img = cv2.resize(crop_img, (48, 48))
		
		im2arr = crop_img[:, :, 0]
		#cv2.imshow('Window',im2arr)
		#cv2.waitKey(0)
		imagesArray.append(im2arr)
		
	imagesArray = np.asarray(imagesArray)
	imagesArray = (imagesArray / 255.0)
	
	posCount = 0
	realPosCount = 0
	realNegCount = 0
	prediction = classification_model.predict(imagesArray)
	for arr in prediction:
		plt.plot([arr[NEED_CLASS]], [randint(total - len(prediction), total)], marker=".", markersize=4)
		mx = np.argmax(arr)
		if mx == NEED_CLASS:
			posCount = posCount + 1
		if arr[NEED_CLASS] > LOWEST_BORDER:
			realPosCount = realPosCount + 1
		if arr[1-NEED_CLASS] > LOWEST_BORDER:
			realNegCount = realNegCount + 1
	
	print((key)*inPacket, "-", (key+1)*inPacket, "total:", len(forPredict[key]), "true:",posCount," (",int(posCount/len(forPredict[key])*100), "%); true with ",LOWEST_BORDER,"%+", realPosCount,"(",int(realPosCount/len(forPredict[key])*100), "%); false with ",LOWEST_BORDER,"%:", realNegCount,"(",int(realNegCount/len(prediction)*100), "%)")
	totalPosCount = totalPosCount + posCount
	totalRealPosCount = totalRealPosCount + realPosCount
	totalRealNegCount = totalRealNegCount + realNegCount

print("по всем. всего:", total, "; true:",totalPosCount,"(",int(totalPosCount/total*100), "%); true with ",LOWEST_BORDER,"%+", totalRealPosCount,"(",int(totalRealPosCount/total*100), "%); false with ",LOWEST_BORDER,"%:", totalRealNegCount,"(",int(totalRealNegCount/total*100), "%)")
print("time for predict just overlapped", time.time() - timeInOverlappedPredict)
plt.axis([0, 1, 0, total])
plt.title('Закрытые')
plt.show()

#============================================



print("открытые:")
timeInOpenPredict = time.time()
imagesArray = []
for row in forPredictNormal:
	img = cv2.imread(RESOURCES + "frames/" + sample +"/"+ row["filename"])
	y,h,x,w = int(row["y_from"]),int(row["height"]),int(row["x_from"]),int(row["width"])
	
	crop_img = img[y:y+h, x:x+w]
	crop_img = cv2.resize(crop_img, (48, 48))
	
	im2arr = crop_img[:, :, 0]
	imagesArray.append(im2arr)
	
imagesArray = np.asarray(imagesArray)
imagesArray = (imagesArray / 255.0)

print("time for load images (just open)", time.time() - timeInOpenPredict)

posCount = 0
realPosCount = 0
realNegCount = 0


prediction = classification_model.predict(imagesArray)

plt.figure(1)
plt.axis([0, 1, 0, len(prediction)])
NEED_CLASS = 0
for arr in prediction:
	plt.plot([arr[NEED_CLASS]], [randint(0,len(prediction))], marker=".", markersize=4)
	mx = np.argmax(arr)
	if mx == NEED_CLASS:
		posCount = posCount + 1
	if arr[NEED_CLASS] > LOWEST_BORDER:
		realPosCount = realPosCount + 1
	if arr[1-NEED_CLASS] > LOWEST_BORDER:
		realNegCount = realNegCount + 1
			
print("time for predict just open", time.time() - timeInOpenPredict)

plt.title('Открытые')
plt.show()

print("total:", len(prediction), "; true:",posCount," (",int(posCount/len(prediction)*100), "%); true with ",LOWEST_BORDER,"%+", realPosCount,"(",int(realPosCount/len(prediction)*100), "%); false with ",LOWEST_BORDER,"%:", realNegCount,"(",int(realNegCount/len(prediction)*100), "%)")
