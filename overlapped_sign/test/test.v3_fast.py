import time
import os
import cv2
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt

RESOURCES = "../../" + "resources/"
SAMPLE = "train/"
MODEL_FOLDER = '../../../models/v3/temp/'
CLASS_NAMES = ['Normal', 'Overlapped']
IMG_SIZE = 48

# закидываем открытые
cur_folder = RESOURCES + "rtsd-r1/" + SAMPLE
cur_class = 0


def format_example(filename, label):
    image_string = tf.io.read_file(filename)
    image_decoded = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.cast(image_decoded, tf.float32)
    image = (image / 127.5) - 1
    image = tf.image.resize(image, (IMG_SIZE, IMG_SIZE))
    return image, label


BATCH_SIZE = 32
SHUFFLE_BUFFER_SIZE = 1000


def load_dataset(cur_folder, cur_class):
    images = [os.path.abspath(os.path.join(cur_folder, p)) for p in os.listdir(cur_folder)]
    return tf.data.Dataset.from_tensor_slices(
        (tf.constant(images), tf.constant([cur_class for _ in range(len(images))])))


fig = plt.figure()
columns = 6
rows = 8
count_plts = 1
classification_model = tf.keras.models.load_model('../../models/v3/temp/1590265961942_0.96417_0.96441.h5')
folder = RESOURCES + "frames_4test/overlap"
cur_class = 1
total = 0
totalPosCount = 0
for x in sorted(os.listdir(folder), reverse=True):
    raw_dataset = load_dataset(folder + "/" + x, cur_class)
    dataset = raw_dataset.map(format_example).batch(BATCH_SIZE)

    prediction = classification_model.predict(dataset)
    prediction_class = classification_model.predict_classes(dataset)

    total = total + len(prediction)
    posCount = sum(x[0] > 0 for x in prediction_class)

    key = int(x)
    inPacket = 20
    packet_str = str(key * inPacket) + "-" + str((key + 1) * inPacket)
    print(packet_str)
    print(key * inPacket, "-", (key + 1) * inPacket, "total:", len(prediction), "true:", posCount, " (",
          int(posCount / len(prediction) * 100), "%);")
    totalPosCount = totalPosCount + posCount

    index = 25

    for image, label in raw_dataset.skip(25).take(25):
        if count_plts > columns * rows:
            break
        image_string = tf.io.read_file(image)
        image_decoded = tf.image.decode_jpeg(image_string, channels=3)
        sub_plt = fig.add_subplot(rows, columns, count_plts)
        count_plts = count_plts + 1
        plt.imshow(image_decoded)
        sub_plt.set_title(
            CLASS_NAMES[int(prediction_class[index])] + "(" + str(round(prediction[index][0], 2)) + ")" + "; orig "
            + CLASS_NAMES[cur_class] + " (" + packet_str + ")", fontdict={'fontsize': 8},
            color=('blue' if prediction_class[index] == int(label) else "red")
        )
        if prediction_class[index] == int(label):
            print(prediction[index][0], len(prediction_class))
        index = index + 1
    print(count_plts)

print("по всем. всего:", total, "; true:", totalPosCount, "(", int(totalPosCount / total * 100), "%)")
plt.subplots_adjust(0, 0, 1, 0.98, 0.2, 0.5)
plt.show()