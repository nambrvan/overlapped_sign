import pandas as pd

RESOURCES = "../../resources/"

sample = "train"

path = RESOURCES + "gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
	for fileName in fileNames:
		path = RESOURCES + "gt/" + fileType + "/" + fileName
		full = full.append(pd.read_csv(path))

print(len(full))
print(len(full.loc[full['overlap'] > 0]))

#full = full.groupby("filename")

countOverlapped = 0;
countNormal = 0;
badImgs = set()
imgs = set()
for index, row in full.iterrows():
	roww = data.loc[data['filename'] == row["filename"]]
	ww = str(roww.get("is_all_set")).split()[1]
	if ww != "NaN":
		if int(float(str(roww.get("is_all_set")).split()[1])) == 1:
			imgs.add(row["filename"])
			if row["overlap"] > 0:
				countOverlapped = countOverlapped + 1
			else:
				countNormal = countNormal + 1
		else:
			badImgs.add(row["filename"])

print(len(imgs))
print(len(badImgs))
print(countNormal)
print(countOverlapped)
