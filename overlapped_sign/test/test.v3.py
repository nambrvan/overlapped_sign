import time

import cv2
import numpy as np
import pandas as pd
import tensorflow as tf

LOWEST_BORDER = 0.8;

RESOURCES = "../../resources/"
IMG_SIZE = 48
sample = "train"

path = RESOURCES + "gt/" + sample + "_filenames.txt"
data = pd.read_csv(path)

fileTypes = ["blue_border", "blue_rect", "danger", "main_road", "mandatory", "prohibitory"]
fileNames = ["train_gt.csv"]

full = pd.DataFrame()
for fileType in fileTypes:
    for fileName in fileNames:
        path = RESOURCES + "gt/" + fileType + "/" + fileName
        full = full.append(pd.read_csv(path))

print(len(full))
print(len(full.loc[full['overlap'] > 0]))

countOverlapped = 0;
countNormal = 0
badImgs = set()
imgs = set()

forPredict = {}
forPredictNormal = []
inPacket = 10
for index, row in full.iterrows():
    # if row["width"] < 35 or row["height"] < 35:
    #	continue
    roww = data.loc[data['filename'] == row["filename"]]
    ww = str(roww.get("is_all_set")).split()[1]
    if ww != "NaN":
        if int(float(str(roww.get("is_all_set")).split()[1])) == 1:
            imgs.add(row["filename"])
            if row["overlap"] > 0 and row["overlap"] < 101:
                key = row["overlap"] // inPacket
                if key not in forPredict:
                    forPredict.update({key: pd.DataFrame()})
                forPredict[key] = forPredict[key].append(row);
                countOverlapped = countOverlapped + 1
            elif row["overlap"] == 0:
                forPredictNormal.append(row);
                countNormal = countNormal + 1
        else:
            badImgs.add(row["filename"])

print("bad imgs", len(badImgs))
print("good imgs", len(imgs))

print(countNormal)
print(countOverlapped)

classification_model = tf.keras.models.load_model('../../models/v3/temp/1590265961942_0.96417_0.96441.h5')

timeInOverlappedPredict = time.time()

NEED_CLASS = 1  # overlapped
total = 0
totalPosCount = 0
for key in sorted(forPredict.keys()):
    total = total + len(forPredict[key])

    imagesArray = []
    for index, row in forPredict[key].iterrows():
        img = cv2.imread(RESOURCES + "frames/" + sample + "/" + row["filename"])
        y, h, x, w = int(row["y_from"]), int(row["height"]), int(row["x_from"]), int(row["width"])

        crop_img = img[y:y + h, x:x + w]
        crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))

        img_tensor = tf.convert_to_tensor(crop_img, dtype=tf.float32)
        img_tensor = (img_tensor / 127.5) - 1

        imagesArray.append(img_tensor)

    imagesArray = np.asarray(imagesArray)

    prediction = classification_model.predict(imagesArray)
    prediction1 = classification_model.predict_classes(imagesArray)
    posCount = sum(x[0] > 0 for x in prediction1)

    print((key) * inPacket, "-", (key + 1) * inPacket, "total:", len(forPredict[key]), "true:", posCount, " (",
          int(posCount / len(forPredict[key]) * 100), "%);")
    totalPosCount = totalPosCount + posCount

print("по всем. всего:", total, "; true:", totalPosCount, "(", int(totalPosCount / total * 100), "%)")
print("time for predict just overlapped", time.time() - timeInOverlappedPredict)

# ============================================


print("открытые:")
timeInOpenPredict = time.time()
imagesArray = []
for row in forPredictNormal:
    img = cv2.imread(RESOURCES + "frames/" + sample + "/" + row["filename"])
    y, h, x, w = int(row["y_from"]), int(row["height"]), int(row["x_from"]), int(row["width"])

    crop_img = img[y:y + h, x:x + w]
    crop_img = cv2.resize(crop_img, (IMG_SIZE, IMG_SIZE))
    img_tensor = tf.convert_to_tensor(crop_img, dtype=tf.float32)
    img_tensor = (img_tensor / 127.5) - 1

    imagesArray.append(img_tensor)

imagesArray = np.asarray(imagesArray)

print("time for load images (just open)", time.time() - timeInOpenPredict)

prediction = classification_model.predict(imagesArray)
prediction1 = classification_model.predict_classes(imagesArray)
posCount = sum(x[0] == 0 for x in prediction1)

print("time for predict just open", time.time() - timeInOpenPredict)

print("total:", len(prediction), "; true:", posCount, " (", int(posCount / len(prediction) * 100), "%)")
