IOU - Intersection over Union

TP — true positive, классификатор верно отнёс объект к рассматриваемому классу.
TN — true negative, классификатор верно утверждает, что объект не принадлежит к рассматриваемому классу.
FP — false positive, классификатор неверно отнёс объект к рассматриваемому классу.
FN — false negative, классификатор неверно утверждает, что объект не принадлежит к рассматриваемому классу.

AP - Average Precision
mAP (средняя средняя точность) – это среднее значение AP. мы вычисляем AP для каждого класса и усредняем их
===============

::PRECISION::
precision = TP / (TP + FP) - показывает долю объектов класса среди объектов выделенных классификатором

mAP, tag: DetectionBoxes_Precision/mAP --- соответствует среднему AP для нескольких IOU от 0,5 до 0,95 с размером шага 0,05
= 0.167

mAP (large), tag: DetectionBoxes_Precision/mAP (large) --- среднее AP для больших объектов (96^2 pixels < area < 10000^2 pixels)
= 0.526

mAP (medium), tag: DetectionBoxes_Precision/mAP (medium) --- среднее AP для средних объектов (32^2 pixels < area < 96^2 pixels)
= 0.254

mAP (small), tag: DetectionBoxes_Precision/mAP (small) --- среднее AP для маленьких объектов (area < 32^2 pixels)
= 0.103

mAP@.50IOU, tag: DetectionBoxes_Precision/mAP@.50IOU --- среднее AP для 50% IOU
= 0.403

mAP@.75IOU, tag: DetectionBoxes_Precision/mAP@.75IOU --- среднее AP для 75% IOU
= 0.108

::RECALL::
recall = TP / (TP + FN) - показывает долю найденных объектов класса к общему числу объектов класса. показывает то, насколько хорошо наш классификатор находит объекты из класса. Например, мы можем находить 80% от всех возможных положительных случаев в наших К лучших предсказаниях.

AR@1, tag: DetectionBoxes_Recall/AR@1 --- (max 1) AR дает 1 обнаружение на изображение
= 0.178

AR@10, tag: DetectionBoxes_Recall/AR@10 --- (max 10) AR дает 10 обнаружение на изображение
= 0.359

AR@100, tag: DetectionBoxes_Recall/AR@100 --- (max 100) AR дает 100 обнаружение на изображение
= 0.398

AR@100 (large), tag: DetectionBoxes_Recall/AR@100 (large) --- AR для больших объектов с 100 обнаружениями
= 0.657

AR@100 (medium), tag: DetectionBoxes_Recall/AR@100 (medium) --- AR для средних объектов с 100 обнаружениями
= 0.460

AR@100 (small), tag: DetectionBoxes_Recall/AR@100 (small) ---  AR для маленьких объектов с 100 обнаружениями
= 0.334


The losses for the Final Classifier:

BoxClassifierLoss/classification_loss, tag: Loss/BoxClassifierLoss/classification_loss ---  потеря для классификации обнаруженных объектов на различные классы (закрыт/не закрыт)
= 0.076

BoxClassifierLoss/localization_loss, tag: Loss/BoxClassifierLoss/localization_loss --- функция потерь для bounding boxes объекта (координаты знаков)
= 0.056


The losses for the Region Proposal Network:

RPNLoss/localization_loss, tag: Loss/RPNLoss/localization_loss --- функция потерь для bounding boxes сгенерированного объекта
= 0.023

RPNLoss/objectness_loss, tag: Loss/RPNLoss/objectness_loss --- классифицирует, BB является интнресующим объектом или фоном 
= 0.038

total_loss, tag: Loss/total_loss
= 0.195

global_step = 17338 - было отработано шагов

learning_rate = 0.0002

loss = 0.195